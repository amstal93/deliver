<?php

namespace Database\Seeders;

use App\Models\CorredorCorrida;
use App\Models\CorredorProva;
use App\Models\Prova;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;


class CorredoresSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('pt_BR');

        for ($i = 0; $i <= 4; $i++) {

            $km = array(3, 5, 10, 21, 42);

            DB::table('provas')->insert([
                'tipo_prova' => $km[$i],
                'data' => $faker->dateTimeBetween('now', '+05 days')->format('Y-m-d'),
            ]);
        }

        for ($i = 1; $i <= 15; $i++) {

            DB::table('corredores_corridas')->insert([
                'nome' => $faker->name,
                'cpf' => $faker->cpf,
                'dt_nascimento' => $faker->dateTimeBetween('1951-01-01', '2003-12-31'),
            ]);
        }

        for ($i = 1; $i <= 15; $i++) {

            DB::table('corredores_provas')->insert([
                'corredores_corridas_id' => CorredorCorrida::pluck('id')[$faker->numberBetween(2, CorredorCorrida::count() - 1)],
                'provas_id' => Prova::pluck('id')[$faker->numberBetween(2, Prova::count() - 1)],
            ]);
        }

        for ($i = 1; $i <= 15; $i++) {

            $startDate = Carbon::createFromTimeStamp($faker->dateTimeBetween('-1 years', '+1 month')->getTimestamp());

            DB::table('resultados')->insert([
                'corredores_corridas_id' => CorredorCorrida::pluck('id')[$faker->numberBetween(1, CorredorCorrida::count() - 1)],
                'provas_id' => Prova::pluck('id')[$faker->numberBetween(1, Prova::count() - 1)],
                'hr_inicio_prova' => $startDate->toDateTimeString(),
                'hr_conclusao_prova' => $startDate->addHours($faker->numberBetween(1, 8)),
            ]);
        }
    }
}
