<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Resultado extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'resultados';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'corredores_corridas_id',
        'provas_id',
        'hr_inicio_prova',
        'hr_conclusao_prova',
    ];

    public function corredoresCorridas()
    {
        return $this->belongsTo('App\Models\CorredorCorrida');
    }

    public function provas()
    {
        return $this->belongsTo('App\Models\Prova');
    }

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public static function getPorOrdemIdade($date1, $date2, $prova) {

        $sql = "SELECT
            @posicao:=
            CASE
                WHEN @grupo<> r.provas_id THEN 1
                WHEN CAST(@tempo_gasto AS TIME) = r.hr_conclusao_prova THEN @posicao
                ELSE @posicao + 1
            END
            AS posicao,
            CASE
                WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) < 18 THEN '18'
                WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) BETWEEN 18 AND 25 THEN '18 - 29'
                WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) BETWEEN 25 AND 35 THEN '25 - 35'
                WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) BETWEEN 35 AND 45 THEN '35 - 45'
                WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) BETWEEN 45 AND 55 THEN '45 - 55'
                WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) >= 55 THEN 'Acima de 55'
                WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) IS NULL THEN ''
            END AS colocacoes,
        @grupo := r.provas_id as 'id_da_prova',
        CONCAT(p.tipo_prova,' Km') as 'tipo_de_prova',
        r.corredores_corridas_id as 'id_do_corredor',
        CONCAT(YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))),' anos') AS idade,
        cc.nome AS 'nome_do_corredor'
        FROM resultados r
        LEFT join corredores_corridas cc on cc.id = r.corredores_corridas_id
        LEFT join provas p on p.id = r.provas_id
        WHERE r.provas_id = $prova->id AND (YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) BETWEEN '$date1' AND '$date2')
        ORDER BY r.provas_id, r.hr_conclusao_prova;";

        DB::statement("SET @posicao:=0;");
        DB::statement("SET @grupo:='';");
        DB::statement("SET @tempo_gasto:=cast('00:00:00' AS time);");

        return DB::select(DB::raw($sql));

    }
}
